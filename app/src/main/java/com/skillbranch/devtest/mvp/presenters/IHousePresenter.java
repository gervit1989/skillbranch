package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.mvp.views.IHouseView;

public interface IHousePresenter {

    /**
     *  проинициализировать экземпляр View
     */
    void takeView(IHouseView house_view);

    /**
     *  отвязать  экземпляр View
     */
    void dropView();

    /**
     *  проинициализировать состояние View (onLoad)
     */
    void initView(int houseNumber);

    @Nullable
    IHouseView getView();
}
