package com.skillbranch.devtest.mvp.views;

import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.presenters.IProfilePresenter;

public interface IProfileView {
    /**
     * Показать сообщение
     * @param message сообщение
     */
    void showMessage(String message);

    /**
     * Показать ошибку
     * @param e - ошибка
     */
    void showError(Throwable e);

    /**
     * Presenter
     * @return Presenter
     */
    IProfilePresenter getPresenter();

    /**
     * Показать Кнопку ОТЕЦ
     */
    void showFatherBtn();

    /**
     * Скрыть Кнопку ОТЕЦ
     */
    void hideFatherBtn();

    /**
     * Показать Кнопку МАТЬ
     */
    void showMotherBtn();

    /**
     * Скрыть Кнопку МАТЬ
     */
    void hideMotherBtn();

    /**
     * Установка тулбара
     */
    void setupToolbar();

    /**
     * Установка содержимого
     */
    void setContent(Hero hero_data, String fatherUser, String motherUser);

    /**
     * Установка панели
     */
    void setCollapsingToolbarLayout();

    /**
     * Ид героя
     * @return ID героя в базе
     */
    String getRemoteId();

    /**
     * Ид отца героя
     * @return ид отца героя
     */
    String getFatherRemoteId();

    /**
     * Ид матери героя
     * @return ид матери героя
     */
    String getMotherRemoteId();

}
