package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.mvp.models.MainModel;
import com.skillbranch.devtest.mvp.views.IMainView;

public class MainPresenter implements IMainPresenter{

    /**
     * Паттерн синглтон
     */
    private static MainPresenter ourInstance = new MainPresenter();

    @Override
    public void setPagerItem(String houseName) {
        if (getView()!=null){
            getView().setPagerItem(houseName);
        }
    }

    @Override
    public void setPagerItem(int iItem) {
        if (getView()!=null){
            getView().setPagerItem(iItem);
        }
    }

    /**
     * Модель заполнения информации о списках пользователей
     */
    private MainModel mMainModel;

    /**
     * View  информации о списках пользователей
     */
    private IMainView mHeroListView;

    /**
     * Конструктор
     */
    public MainPresenter() {
        mMainModel = new MainModel();
    }

    /**
     * Возвращаем сами себя
     * @return сами себя
     */
    public static MainPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IMainView hero_list_view) {
        mHeroListView = hero_list_view;
    }

    @Override
    public void dropView() {
        mHeroListView = null;
    }

    @Override
    public void initView() {
        if (getView()!=null) {
            getView().setupToolbar();
            getView().setupViewPager();
            getView().setupDrawer();
        }
    }

    @Nullable
    @Override
    public IMainView getView() {
        return mHeroListView;
    }
}
