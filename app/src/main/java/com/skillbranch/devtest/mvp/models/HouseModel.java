package com.skillbranch.devtest.mvp.models;

import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.storage.models.Hero;

import java.util.List;

public class HouseModel {

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    /**
     * Конструктор
     */
    public HouseModel() {

        /**
         * Менеджер данных
         */
        mDataManager = DataManager.getINSTANCE();
    }

    /**
     * Снять список героев дома
     * @param houseNumber номер дома
     * @return список героев
     */
    public List<Hero> getHeroListInfo(int houseNumber) {
        String sHouseName = mDataManager.getPreferencesManager().loadHouseNames().get(houseNumber);
        return mDataManager.getHeroListFromDb(sHouseName);
    }
}
