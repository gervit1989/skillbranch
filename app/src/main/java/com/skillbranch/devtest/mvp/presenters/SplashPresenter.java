package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.models.SplashModel;
import com.skillbranch.devtest.mvp.views.ISplashView;
import com.skillbranch.devtest.utils.NetworkStatusChecker;

import java.util.ArrayList;
import java.util.List;

public class SplashPresenter implements ISplashPresenter{

    /**
     * Паттерн синглтон
     */
    private static SplashPresenter ourInstance = new SplashPresenter();

    /**
     * Модель заполнения информации о списках пользователей
     */
    private SplashModel mSplashModel;

    /**
     * View  информации о списках пользователей
     */
    private ISplashView mSplashView;

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;


    /**
     * список персонажей
     */
    private List<Hero> mHeroList;

    public boolean isLoad() {
        return isLoad;
    }

    private boolean isLoad;

    /**
     * Конструктор
     */
    public SplashPresenter() {
        isLoad = false;
        mHeroList = new ArrayList<>();
        mSplashModel = new SplashModel();
        mDataManager = DataManager.getINSTANCE();
    }

    /**
     * Возвращаем сами себя
     * @return сами себя
     */
    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {
        if (getView()!=null) {
            if (NetworkStatusChecker.isNetworkAvailable(mDataManager.getContext())) {
                run_Activity();
                getView().startMainActivityFromSplash(isLoad());
            }
        }
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public void run_Activity() {
        if (getView()!=null) {
            mHeroList = mSplashModel.processData();
        }
    }

    public List<Hero> getHeroList() {
        return mHeroList;
    }
}
