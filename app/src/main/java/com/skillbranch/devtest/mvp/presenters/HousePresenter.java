package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.models.HouseModel;
import com.skillbranch.devtest.mvp.views.IHouseView;

import java.util.List;

public class HousePresenter implements IHousePresenter{

    /**
     * Паттерн синглтон
     */
    private static HousePresenter ourInstance = new HousePresenter();

    /**
     * Модель заполнения информации о списках пользователей
     */
    private HouseModel mHouseModel;

    /**
     * View  информации о списках пользователей
     */
    private IHouseView mHouseView;

    /**
     * Конструктор
     */
    public HousePresenter() {
        mHouseModel = new HouseModel();
    }

    /**
     * Возвращаем сами себя
     * @return  сами себя
     */
    public static HousePresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IHouseView house_view) {
        mHouseView = house_view;
    }

    @Override
    public void dropView() {
        mHouseView = null;
    }

    @Override
    public void initView(int houseNumber) {
        if (getView()!=null) {
            getView().setupRecyclerView();
            List<Hero> membersList = mHouseModel.getHeroListInfo
                    (houseNumber);
            getView().setupRecyclerViewAdapter(membersList);
        }
    }

    @Nullable
    @Override
    public IHouseView getView() {
        return mHouseView;
    }
}
