package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;
import com.skillbranch.devtest.mvp.views.ISplashView;

public interface ISplashPresenter {

    /**
     *  проинициализировать экземпляр View
     */
    void takeView(ISplashView splashView);

    /**
     *  отвязать  экземпляр View
     */
    void dropView();

    /**
     *  проинициализировать состояние View (onLoad)
     */
    void initView();

    @Nullable
    ISplashView getView();

    /**
     * Отложенный запуск Activity
     */
    void run_Activity();
}
