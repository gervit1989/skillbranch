package com.skillbranch.devtest.mvp.views;

import com.skillbranch.devtest.mvp.presenters.IMainPresenter;

public interface IMainView {
    /**
     * Показать сообщение
     * @param message сообщение
     */
    void showMessage(String message);

    /**
     * Показать ошибку
     * @param e - ошибка
     */
    void showError(Throwable e);

    /**
     * Установка тулбара
     */
    void setupToolbar();

    /**
     * Установка выдвижного меню
     */
    void setupDrawer();

    /**
     * Установка ViewPager
     */
    void setupViewPager();

    /**
     * Presenter
     * @return Presenter
     */
    IMainPresenter getPresenter();

    void setPagerItem(int iItem);

    void setPagerItem(String houseName);
}
