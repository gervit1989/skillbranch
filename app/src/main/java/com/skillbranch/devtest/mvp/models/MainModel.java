package com.skillbranch.devtest.mvp.models;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;

import java.util.ArrayList;
import java.util.List;

public class MainModel {
    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    /**
     * Конструктор
     */
    public MainModel() {

        /**
         * Менеджер данных
         */
        mDataManager = DataManager.getINSTANCE();
    }
}
