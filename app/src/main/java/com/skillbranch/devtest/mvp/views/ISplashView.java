package com.skillbranch.devtest.mvp.views;

import com.skillbranch.devtest.mvp.presenters.ISplashPresenter;

public interface ISplashView {
    /**
     * Показать сообщение
     * @param message сообщение
     */
    void showMessage(String message);

    /**
     * Показать ошибку
     * @param e - ошибка
     */
    void showError(Throwable e);

    /**
     * Показать загрузку
     */
    void showLoad();

    /**
     * Скрыть загрузку
     */
    void hideLoad();

    /**
     * Отложенный запуск Activity
     */
    void startMainActivityFromSplash(boolean bPar);

    /**
     * Presenter
     * @return Presenter
     */
    ISplashPresenter getPresenter();

}
