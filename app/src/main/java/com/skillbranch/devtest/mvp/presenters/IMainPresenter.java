package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.mvp.views.IMainView;

public interface IMainPresenter {

    /**
     *  проинициализировать экземпляр View
     */
    void takeView(IMainView hero_list_view);

    /**
     *  отвязать  экземпляр View
     */
    void dropView();

    /**
     *  проинициализировать состояние View (onLoad)
     */
    void initView();

    @Nullable
    IMainView getView();

    void setPagerItem(int iItem);

    void setPagerItem(String houseName);
}
