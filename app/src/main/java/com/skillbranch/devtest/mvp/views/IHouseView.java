package com.skillbranch.devtest.mvp.views;

import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.presenters.IHousePresenter;

import java.util.List;

public interface IHouseView {
    /**
     * Показать сообщение
     * @param message сообщение
     */
    void showMessage(String message);

    /**
     * Показать ошибку
     * @param e - ошибка
     */
    void showError(Throwable e);

    /**
     * Заполнить итемы
     * @param heroList итемы
     */
    void setupRecyclerViewAdapter(List<Hero> heroList);

    /**
     * Установка RecyclerView
     */
    void setupRecyclerView();

    /**
     * Получить номер дома
     * @return номер дома
     */
    int getHouseNumber();

    /**
     * Presenter
     * @return Presenter
     */
    IHousePresenter getPresenter();
}
