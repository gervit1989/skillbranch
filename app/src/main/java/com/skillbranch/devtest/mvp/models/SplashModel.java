package com.skillbranch.devtest.mvp.models;

import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.network.res.HeroModelResponse;
import com.skillbranch.devtest.data.network.res.HouseModelResponse;
import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.presenters.SplashPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashModel {

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    /**
     * Presenter
     */
    SplashPresenter mPresenter;

    /**
     * Массивы данных
     */
    private List<Integer> mHouseList;
    private List<String> mHouseHeroList;
    private List<Integer> mHouseHeroIndexList;

    private List<Hero> allHeroes;
    private List<String> mHouseWords;
    private List<Integer> mHouseListUse;
    private int m_iHouseId;
    private int m_iId;

    /**
     * Конструктор
     */
    public SplashModel() {
        mDataManager = DataManager.getINSTANCE();
        mPresenter = SplashPresenter.getInstance();
    }

    /**
     * Заполнение данных
     *
     * @return списка героев
     */
    public List<Hero> processData() {
        /**
         * Инициализация данных
         */
        // Список ID героев домов
        mHouseHeroList = new ArrayList<>();
        mHouseHeroIndexList = new ArrayList<>();
        mHouseHeroIndexList.add(0);

        // Хранилища информации о героях
        allHeroes = new ArrayList<>();

        // Инициализация списка домов
        mHouseList = new ArrayList<>();
        mHouseListUse = new ArrayList<>();
        mHouseWords = new ArrayList<>();

        mHouseList.add(362);//Stark
        mHouseListUse.add(0);
        mHouseList.add(229);//Lannister
        mHouseListUse.add(1);
        mHouseList.add(230);//Lannister
        mHouseListUse.add(1);
        mHouseList.add(231);//Lannister
        mHouseListUse.add(1);
        mHouseList.add(378);//Targaryens
        mHouseListUse.add(2);
        m_iHouseId = 0;

        /**
         * Прописываем строку адрес дома
         */
        String s = Integer.toString(mHouseList.get(m_iHouseId));


        /**
         * Зачитываем дом
         */
        readHouseFromNetwork(s);
        return allHeroes;
    }

    //region ========================= Network =========================
    /**
     * Считывание дома и заполнение списка героев
     * @param s - адрес дома
     */
    private void readHouseFromNetwork(String s) {
        Call<HouseModelResponse> call = mDataManager.getHouseFromNetwork(s);
        // асинхронный вызов
        call.enqueue(new Callback<HouseModelResponse>() {
            @Override
            public void onResponse(Call<HouseModelResponse> call, Response<HouseModelResponse> response) {
                if (response.code() == 200) {
                    if (!response.body().getWords().isEmpty()) {
                        mHouseWords.add(response.body().getWords());
                    }
                    Integer iVal = mHouseListUse.get(m_iHouseId);
                    for (int i = 0; i < response.body().getSwornMembers().size(); i++) {
                        String sIdStr = getHeroIdByAddress(response.body().getSwornMembers().get(i));
                        mHouseHeroList.add(sIdStr);
                    }
                    if (m_iHouseId>0) {
                        if (mHouseListUse.get(m_iHouseId-1).equals(mHouseListUse.get(m_iHouseId)))
                        {
                            mHouseHeroIndexList.set(mHouseHeroIndexList.size()-1, mHouseHeroList.size());
                        }else {
                            mHouseHeroIndexList.add(mHouseHeroList.size());
                        }
                    }
                    else {
                        mHouseHeroIndexList.add(mHouseHeroList.size());
                    }
                    m_iHouseId++;
                    if (m_iHouseId == mHouseList.size()) {
                        m_iId = 0;
                        m_iHouseId = 0;
                        String s = mHouseHeroList.get(m_iId);
                        readHeroFromNetwork(s);
                    } else {
                        String s = Integer.toString(mHouseList.get(m_iHouseId));
                        readHouseFromNetwork(s);
                    }
                } else {
                    showMessage("Не существует дома!!!");
                }
            }

            @Override
            public void onFailure(Call<HouseModelResponse> call, Throwable t) {
                // TODO: 10.07.2016 Обработать ошибки
                showMessage("Ошибка соединения!!!");
            }
        });
    }

    /**
     * Зачитка героя
     * @param s адрес героя в сети
     */
    private void readHeroFromNetwork(String s) {
        Call<HeroModelResponse> call = mDataManager.getHeroFromNetwork(s);
        // асинхронный вызов
        call.enqueue(new Callback<HeroModelResponse>() {
            @Override
            public void onResponse(Call<HeroModelResponse> call, Response<HeroModelResponse> response) {
                if (response.code() == 200) {
                    if(m_iId < mHouseHeroList.size()){
                        add_hero(response.body());
                        m_iId++;
                    }
                } else {
                    showMessage("Герой не найден!!!");
                }
            }

            @Override
            public void onFailure(Call<HeroModelResponse> call, Throwable t) {
                // TODO: 10.07.2016 Обработать ошибки
                showMessage("Ошибка соедиенения!!!");
            }
        });
    }

    /**
     * Добавить героя из модели
     * @param body модель
     */
    private void add_hero(HeroModelResponse body) {
        if (m_iId >= mHouseHeroIndexList.get(m_iHouseId+1))
            m_iHouseId++;
        Hero  _hero = new Hero(body, getHeroIdByAddress(body.getUrl()), mHouseWords.get(m_iHouseId));
        _hero.setHouseName(mDataManager.getPreferencesManager().loadHouseNames().get(m_iHouseId));
        if(_hero.getFather()!=null){
            if(!isExistHero(_hero.getFather())) {
            }
            _hero.setFather(getHeroIdByAddress(_hero.getFather()));
        }
        if(_hero.getMother()!=null){
            if(!isExistHero(_hero.getMother())) {
            }
            _hero.setMother(getHeroIdByAddress(_hero.getMother()));
        }
        allHeroes.add(_hero);
        if (m_iId==mHouseHeroList.size()-1){
            mPresenter = SplashPresenter.getInstance();
            if (mPresenter!=null) {
                if (mPresenter.getView() != null) {
                    mPresenter.getView().startMainActivityFromSplash(true);
                }
            }
        }
        else {
            String sHeroNextId = mHouseHeroList.get(m_iId);
            readHeroFromNetwork(sHeroNextId);
        }
    }
    //endregion

    //region ========================= Auxiliary =========================

    /**
     * Возвращаем строку с ID героя
     *
     * @param sHero - адрес героя
     * @return строка с ID
     */
    private String getHeroIdByAddress(String sHero) {
        String[] sSplit = sHero.split("/");
        return sSplit[sSplit.length - 1];
    }

    /**
     * Определяем, существует ли герой в базе
     *
     * @param sHero - адрес героя
     * @return существует ли?
     */
    private boolean isExistHero(String sHero) {
        // ID героя
        String sReqHero = getHeroIdByAddress(sHero);

        // По дому Старков
        for (String s : mHouseHeroList
                ) {
            if (sReqHero.equals(s)) {
                return true;
            }
        }
        return false;
    }
    //endregion

    //region ========================= View =========================
    public void showMessage(String s){
        if (mPresenter.getView()!=null){
            mPresenter.getView().showMessage(s);
        }
    }
    //endregion
}
