package com.skillbranch.devtest.mvp.models;

import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.storage.models.Hero;

public class ProfileModel {

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    /**
     * Конструктор
     */
    public ProfileModel() {

        /**
         * Менеджер данных
         */
        mDataManager = DataManager.getINSTANCE();
    }

    /**
     * Достаем героя из базы по id
     * @param remoteId id героя
     * @return героя
     */
    public Hero getHero(String remoteId) {
        return mDataManager.getHeroFromDbByID(remoteId);
    }


}
