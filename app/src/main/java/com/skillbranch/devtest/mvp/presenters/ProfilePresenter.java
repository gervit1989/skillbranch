package com.skillbranch.devtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.models.ProfileModel;
import com.skillbranch.devtest.mvp.views.IProfileView;

public class ProfilePresenter implements IProfilePresenter {

    /**
     * Паттерн синглтон
     */
    private static ProfilePresenter ourInstance = new ProfilePresenter();

    /**
     * Модель заполнения информации о пользователе
     */
    private ProfileModel mHeroProfileModel;

    /**
     * View  информации о пользователе
     */
    private IProfileView mHeroProfileView;


    /**
     * Конструктор
     */
    public ProfilePresenter() {
        mHeroProfileModel = new ProfileModel();
    }

    /**
     * Возвращаем сами себя
     * @return  сами себя
     */
    public static ProfilePresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IProfileView profileView) {
        mHeroProfileView = profileView;
    }

    @Override
    public void dropView() {
        mHeroProfileView = null;
    }

    @Override
    public void initView() {
        if (getView()!=null) {
            getView().setupToolbar();
            getView().setCollapsingToolbarLayout();
            Hero hero = mHeroProfileModel.getHero( getView().getRemoteId());
            String sFather="";
            String sMother="";
            String s = hero.getFather();
            if (s!=null && s.length()!=0){
                Hero h = mHeroProfileModel.getHero( s );
                sFather = h.getName();
            }
            s = hero.getMother();
            if (s!=null && s.length()!=0){
                Hero h = mHeroProfileModel.getHero( s );
                sMother = h.getName();
            }
            getView().setContent(hero, sFather, sMother);
        }
    }

    @Nullable
    @Override
    public IProfileView getView() {
        return mHeroProfileView;
    }

    @Override
    public void clickOnFather() {
        if (getView()!=null) {
            getView().showMessage("Click on Father");
            String s = getView().getFatherRemoteId();
            if (s != null && s.length() != 0) {
                Hero hero = mHeroProfileModel.getHero(s);
                if (hero!=null){
                    getView().setContent(hero, null, null);
                }
            }
        }
    }

    @Override
    public void clickOnMother() {
        if (getView()!=null) {
            getView().showMessage("Click on Mother");
            String s = getView().getMotherRemoteId();
            if (s != null && s.length() != 0) {
                Hero hero = mHeroProfileModel.getHero(s);
                if (hero!=null){
                    getView().setContent(hero, null, null);
                }
            }
        }
    }
}
