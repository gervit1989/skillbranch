package com.skillbranch.devtest.ui.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.ui.activities.MainActivity;
import com.skillbranch.devtest.ui.activities.ProfileActivity;

import java.util.List;

public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter.HeroViewHolder> {

    private MainActivity mMainActivity;
    private List<String> mHeroes;
    private List<String> mRemoteIds;
    private String mHouseName;

    public HeroAdapter(MainActivity mainActivity, List<String>
            heroes, List<String> remoteIds, String houseName) {
        mMainActivity = mainActivity;
        mHeroes = heroes;
        mRemoteIds = remoteIds;
        mHouseName = houseName;
    }

    @Override
    public HeroAdapter.HeroViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R
                .layout.item_hero_list, parent, false);

        return new HeroViewHolder(convertView, mMainActivity);
    }

    @Override
    public void onBindViewHolder(final HeroAdapter.HeroViewHolder holder, int position) {
        holder.mSwornMemberName.setText(mHeroes.get(position));
        holder.mInfo.setText(R.string.lorem_ipsum);
        holder.remoteId = mRemoteIds.get(position);
        holder.mHouseName = mHouseName;

        int iItem = 0;
        List<String> sHouseList = DataManager.getINSTANCE().getPreferencesManager().loadHouseNames();
        for (int i = 0; i < sHouseList.size(); i++) {
            if (mHouseName.equals(sHouseList.get(i))){
                iItem = i;
                break;
            }
        }
        holder.mHouseLogo.setImageResource(DataManager.getINSTANCE().getPreferencesManager().loadUserIconData()[iItem]);
    }

    @Override
    public int getItemCount() {
        return mHeroes.size();
    }

    public static class HeroViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private MainActivity mActivity;
        private TextView mSwornMemberName;
        private TextView mInfo;
        private ImageView mHouseLogo;
        private String remoteId;
        private String mHouseName;

        public HeroViewHolder(View itemView, MainActivity mainActivity) {
            super(itemView);

            mActivity = mainActivity;
            itemView.setOnClickListener(this);

            mSwornMemberName = (TextView) itemView.findViewById(R.id.sworn_member_name_tv);
            mInfo = (TextView) itemView.findViewById(R.id.info_tv);
            mHouseLogo = (ImageView) itemView.findViewById(R.id.house_logo_iv);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mActivity, ProfileActivity.class);
            intent.putExtra("remoteId", remoteId);
            intent.putExtra("house_name", mHouseName);
            mActivity.startActivity(intent);
        }
    }
}
