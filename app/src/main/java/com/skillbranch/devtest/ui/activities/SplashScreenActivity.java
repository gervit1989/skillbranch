package com.skillbranch.devtest.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.network.res.HeroModelResponse;
import com.skillbranch.devtest.data.network.res.HouseModelResponse;
import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.data.storage.models.HeroDao;
import com.skillbranch.devtest.mvp.presenters.ISplashPresenter;
import com.skillbranch.devtest.mvp.presenters.SplashPresenter;
import com.skillbranch.devtest.mvp.views.ISplashView;
import com.skillbranch.devtest.utils.ConstantManager;

import android.support.annotation.Nullable;
import android.support.design.BuildConfig;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity implements ISplashView {

    /**
     * Время в миллисекундах, в течение которого будет отображаться Splash Screen
     */
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    /**
     * Презентер
     */
    SplashPresenter mPresenter = SplashPresenter.getInstance();

    /**
     * Тег отслеживания
     */
    private static final String TAG = ConstantManager.TAG_PREFIX + "SplashActivity";

    /**
     * Пользовательские настройки
     */
    private DataManager mDataManager;

    /**
     * Репозитории
     */
    private HeroDao mHeroDao;

    /**
     * Координатор
     */
    //@BindView(R.id.splash_coordinator)
    private CoordinatorLayout mCoordinatorLayout;

    //region  ========================= Life cycle =========================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Хранилище данных пользователя
         */
        mDataManager = DataManager.getINSTANCE();

        /**
         * Репозитории
         */
        mHeroDao = mDataManager.getDaoSession().getHeroDao();

        /**
         * Вытсавляем данные
         */
        setContentView(R.layout.splash_screen);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.splash_coordinator);
        /**
         * Снимаем заголовки вкладок
         */
        if (mDataManager.getPreferencesManager().loadHouseNames().size()==0){
            List<String> stringList = new ArrayList<>();
            stringList.add(getResources().getString(R.string.house_name_first));
            stringList.add(getResources().getString(R.string.house_name_second));
            stringList.add(getResources().getString(R.string.house_name_third));
            mDataManager.getPreferencesManager().saveUserData(stringList);
        }

        /**
         * Снимаем логотипы вкладок
         */
        if (mDataManager.getPreferencesManager().loadUserPicData().length==0) {
            int[] imageArray = new int[]{ R.drawable.stark_logo, R.drawable.lannister_logo, R.drawable.targaryen_logo};
            mDataManager.getPreferencesManager().saveUserPicData(imageArray);
        }

        /**
         * Снимаем иконки вкладок
         */
        if (mDataManager.getPreferencesManager().loadUserIconData().length==0) {
            int[] iconArray = new int[]{ R.drawable.item_icon_stark, R.drawable.item_icon_lannister, R.drawable.item_icon_targaryen};
            mDataManager.getPreferencesManager().saveUserIconData(iconArray);
        }

        mPresenter.takeView(this);
        mPresenter.initView();
    }
    //endregion

    //region ========================= ISplashView =========================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }
        else {
            showMessage("Ошибка! Что то не так!");
            // TODO: 22-10-2016 send error  stacktrace
        }
    }

    @Override
    public void showLoad() {
        FrameLayout progressLay = (FrameLayout) findViewById(R.id.progress_lay);
        if(progressLay!=null){
            progressLay.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void hideLoad() {
        FrameLayout progressLay = (FrameLayout) findViewById(R.id.progress_lay);
        if(progressLay!=null){
            progressLay.setVisibility(View.INVISIBLE);
        }
    }

    private void startMain(){
        mHeroDao.insertOrReplaceInTx(mPresenter.getHeroList());
        // По истечении времени, запускаем главный активити, а Splash Screen закрываем
        Intent mainIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }
    @Override
    public void startMainActivityFromSplash(boolean bLoad) {
        if(bLoad) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startMain();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }
    //endregion

}
