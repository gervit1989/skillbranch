package com.skillbranch.devtest.ui.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.BuildConfig;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.mvp.presenters.IMainPresenter;
import com.skillbranch.devtest.mvp.presenters.MainPresenter;
import com.skillbranch.devtest.mvp.views.IMainView;
import com.skillbranch.devtest.ui.adapters.ViewPagerAdapter;
import com.skillbranch.devtest.ui.fragments.TabFragment;
import com.skillbranch.devtest.utils.ConstantManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMainView{

    /**
     * Тег отслеживания
     */
    private static final String TAG = ConstantManager.TAG_PREFIX + " MainActivity";

    /**
     * Presenter
     */
    IMainPresenter mPresenter = MainPresenter.getInstance();

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    @BindView(R.id.main_coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.main_viewpager)
    ViewPager mViewPager;

    @BindView(R.id.main_tabs)
    TabLayout mTabLayout;

    @BindView(R.id.main_navigation_drawer)
    DrawerLayout mNavigationDrawer;

    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_list);
        ButterKnife.bind(this);

        /**
         * Менеджер данных
         */
        mDataManager = DataManager.getINSTANCE();

        mPresenter.takeView(this);
        mPresenter.initView();

        mTabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mNavigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Обработка нажатия кнопки "back". Убирает открытую NavigationDrawer
     */
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        if (mNavigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mNavigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    //region ========================= IMainView =========================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }
        else {
            showMessage("Ошибка! Что то не так!");
            // TODO: 22-10-2016 send error  stacktrace
        }
    }

    @Override
    public void setPagerItem(int iItem) {
        mViewPager.setCurrentItem(iItem, true);
    }

    @Override
    public void setPagerItem(String houseName) {
        int iItem = 0;
        List<String> sHouseList = mDataManager.getPreferencesManager().loadHouseNames();
        for (int i = 0; i < sHouseList.size(); i++) {
            if (houseName.equals(sHouseList.get(i))){
                iItem = i;
                break;
            }
        }

        mViewPager.setCurrentItem(iItem, true);
    }

    @Override
    public void setupToolbar() {
        Log.d(TAG, "setupToolbar");

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    @Override
    public void setupDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView
                .OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                String sMenuItemTitle = item.getTitle().toString();
                setPagerItem(sMenuItemTitle);
                mNavigationDrawer.closeDrawer(GravityCompat.START);

                return false;
            }
        });
    }

    @Override
    public void setupViewPager() {

        /**
         * Список имен
         */
        List<String> tabList = mDataManager.getPreferencesManager().loadHouseNames();
        if (tabList.size()>0){
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            for (int i = 0; i < tabList.size(); i++) {
                adapter.addFragment(TabFragment.newInstance(i),
                        tabList.get(i));
            }
            mViewPager.setAdapter(adapter);
        }

    }

    @Override
    public IMainPresenter getPresenter() {
        return mPresenter;
    }
    //endregion

}
