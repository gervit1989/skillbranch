package com.skillbranch.devtest.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.BuildConfig;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.presenters.HousePresenter;
import com.skillbranch.devtest.mvp.presenters.IHousePresenter;
import com.skillbranch.devtest.mvp.views.IHouseView;
import com.skillbranch.devtest.ui.activities.MainActivity;
import com.skillbranch.devtest.ui.adapters.HeroAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabFragment extends Fragment implements IHouseView{

    /**
     * Presenter
     */
    IHousePresenter mPresenter = HousePresenter.getInstance();

    private CoordinatorLayout mCoordinatorLayout;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private HeroAdapter mHeroAdapter;

    private int mHouseNumber;

    /**
     * Конструктор
     */
    public TabFragment() {
    }

    public static TabFragment newInstance(int houseNumber) {
        Bundle bundle = new Bundle();
        bundle.putInt("houseNumber", houseNumber);

        TabFragment fragment = new TabFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_fragment, container,
                false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mCoordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.main_coordinator_layout);

        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView(mHouseNumber);

        return rootView;
    }

    @Override
    public int getHouseNumber() {
        return mHouseNumber;
    }

    private void readBundle(Bundle arguments) {
        if (arguments != null) {
            mHouseNumber = arguments.getInt("houseNumber");
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }
        else {
            showMessage("Error!");
            // TODO: 22-10-2016 send error  stacktrace
        }
    }

    @Override
    public void setupRecyclerViewAdapter(List<Hero> heroList) {

        List<String> names = new ArrayList<>();
        List<String> remoteIds = new ArrayList<>();

        for (Hero member : heroList) {
            names.add(member.getName());
            remoteIds.add(member.getRemoteId());
        }
        String sHouseName = DataManager.getINSTANCE().getPreferencesManager().loadHouseNames().get(mHouseNumber);
        mHeroAdapter = new HeroAdapter((MainActivity) getActivity(),
                names, remoteIds, sHouseName);
        mRecyclerView.setAdapter(mHeroAdapter);
    }

    @Override
    public void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public IHousePresenter getPresenter() {
        return mPresenter;
    }
}
