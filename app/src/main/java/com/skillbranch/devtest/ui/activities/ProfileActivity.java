package com.skillbranch.devtest.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.BuildConfig;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skillbranch.devtest.R;
import com.skillbranch.devtest.data.managers.DataManager;
import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.mvp.presenters.IProfilePresenter;
import com.skillbranch.devtest.mvp.presenters.MainPresenter;
import com.skillbranch.devtest.mvp.presenters.ProfilePresenter;
import com.skillbranch.devtest.mvp.views.IProfileView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements IProfileView, View.OnClickListener{
    /**
     * Presenter
     */
    IProfilePresenter mPresenter = ProfilePresenter.getInstance();

    @BindView(R.id.profile_coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.words_content)
    TextView mWords;

    @BindView(R.id.born_content)
    TextView mBorn;

    @BindView(R.id.titles_content)
    TextView mTitles;

    @BindView(R.id.aliases_content)
    TextView mAliases;

    @BindView(R.id.father_name)
    TextView mFather;

    @BindView(R.id.mother_name)
    TextView mMother;

    @BindView(R.id.m_label)
    TextView mMotherLabel;

    @BindView(R.id.f_label)
    TextView mFatherLabel;

    @BindView(R.id.profile_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.profile_collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.house_image)
    ImageView mHouseImage;

    private String mRemoteId;
    private String mMotherRemoteId;
    private String mFatherRemoteId;
    private String mDied;
    private int mHouseNumber;

    /**
     * Менеджер данных
     */
    private DataManager mDataManager;

    //region  ========================= Life cycle =========================
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile_hero);
        ButterKnife.bind(this);

        mDataManager = DataManager.getINSTANCE();
        mRemoteId = getIntent().getStringExtra("remoteId");
        mHouseNumber = getIntent().getExtras().getInt("houseNumber");

        mHouseImage.setImageResource(mDataManager.getPreferencesManager().loadUserPicData()[mHouseNumber]);

        mDied = "";

        mPresenter.takeView(this);
        mPresenter.initView();
        mFather.setOnClickListener(this);
        mMother.setOnClickListener(this);
    }
    //endregion

    //region ========================= IProfileView =========================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }
        else {
            showMessage("Ошибка! Что то не так!");
            // TODO: 22-10-2016 send error  stacktrace
        }
    }

    @Override
    public IProfilePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showFatherBtn() {
        mFather.setVisibility(View.VISIBLE);
        mFatherLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFatherBtn() {
        mFather.setVisibility(View.GONE);
        mFatherLabel.setVisibility(View.GONE);
    }

    @Override
    public String getRemoteId() {
        return mRemoteId;
    }

    @Override
    public void showMotherBtn() {
        mMother.setVisibility(View.VISIBLE);
        mMotherLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMotherBtn() {
        mMother.setVisibility(View.GONE);
        mMotherLabel.setVisibility(View.GONE);
    }

    @Override
    public void setupToolbar() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setCollapsingToolbarLayout() {
        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        mCollapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
    }

    @Override
    public void setContent(Hero hero_data, String fatherUser, String motherUser) {
        String s = hero_data.getFather();
        if (s!=null && s.length()!=0){
            hero_data.setFather(fatherUser);
            mFatherRemoteId = s;
        }
        s = hero_data.getMother();
        if (s!=null && s.length()!=0){
            hero_data.setFather(motherUser);
            mMotherRemoteId = s;
        }
        setContent(hero_data);
    }

    @Override
    public String getFatherRemoteId() {
        return mFatherRemoteId;
    }

    @Override
    public String getMotherRemoteId() {
        return mMotherRemoteId;
    }

    public void setContent(Hero hero_data) {
        mWords.setText(setInfo(hero_data.getHouseWords()));
        mBorn.setText(setInfo(hero_data.getBorn()));
        mTitles.setText(setInfo(hero_data.getTitles()));
        String s =setInfo(hero_data.getAliases());
        mAliases.setText(s);

        mDied = hero_data.getDied().trim();

        if (mDied.length() != 0 && mDied!=null) {
            Snackbar.make(mCoordinatorLayout, "I died " + mDied, Snackbar
                    .LENGTH_LONG).show();
        }

        if (hero_data.getFather()!=null && hero_data.getFather().length()!=0) {
            showFatherBtn();
            mFather.setText(setParentName(hero_data.getFather()));
        }
        else {
            hideFatherBtn();
        }
        if (hero_data.getMother()!=null && hero_data.getMother().length()!=0) {
            showMotherBtn();
            mMother.setText(setParentName(hero_data.getMother()));
        }
        else {
            hideMotherBtn();
        }
        mHouseNumber = 0;
        List<String> sHouseList = mDataManager.getPreferencesManager().loadHouseNames();
        for (int i = 0; i < sHouseList.size(); i++) {
            if (hero_data.getHouseName().equals(sHouseList.get(i))){
                mHouseNumber = i;
                break;
            }
        }

        mHouseImage.setImageResource(mDataManager.getPreferencesManager().loadUserPicData()[mHouseNumber]);
        mCollapsingToolbarLayout.setTitle(hero_data.getName());
    }
    //endregion

    //region ========================= Private Methods =========================

    private String setInfo(String infoFromDb) {
        if (infoFromDb!=null) {
            if (infoFromDb.trim().length() == 0) {
                return getResources().getString(R.string.error_string);
            }
            return infoFromDb.trim();
        }
        return getResources().getString(R.string.error_string);
    }

    private String setParentName(String infoFromDb) {
        if (infoFromDb!=null) {
            if (infoFromDb.trim().length() == 0) {
                return getResources().getString(R.string.error_string);
            }
            return infoFromDb.trim();
        }
        return getResources().getString(R.string.error_string);
    }

    /**
     * Обработка нажатия кнопки "back". Убирает открытую NavigationDrawer
     */
    @Override
    public void onBackPressed() {
        MainPresenter presenter = MainPresenter.getInstance();
        if (presenter !=null){
            presenter.setPagerItem(mHouseNumber);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            MainPresenter presenter = MainPresenter.getInstance();
            if (presenter !=null){
                presenter.setPagerItem(mHouseNumber);
            }
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.father_name:
                mPresenter.clickOnFather();
                break;
            case R.id.mother_name:
                mPresenter.clickOnMother();
                break;
        }
    }
    //endregion
}
