package com.skillbranch.devtest.utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.skillbranch.devtest.data.storage.models.Hero;
import com.skillbranch.devtest.data.storage.models.HeroDao;

import java.util.ArrayList;
import java.util.List;

public class CustomLoader extends AsyncTaskLoader<List<Hero>> {

    /**
     * Тег отслеживания
     */
    private static final String TAG = ConstantManager.TAG_PREFIX + "CustomLoader";

    /**
     * Имя отслеживаемого дома
     */
    private String sHouseName;

    /**
     * Конструктор
     * @param context
     */
    public CustomLoader(Context context, String sHouseName) {
        super(context);
        this.sHouseName = sHouseName;
        Log.d(TAG,"CustomLoader");
    }

    /**
     * Загрузка списка пользователей в фоновом режиме
     * @return
     */
    @Override
    public List<Hero> loadInBackground() {
        Log.d(TAG,"loadInBackground");
        List<Hero> userList = new ArrayList<>();
        try {
            userList = SkillBranchTestApplication.getDaoSession().queryBuilder(Hero.class)
                    .where(HeroDao.Properties.HouseName.like(sHouseName))
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userList;
    }

    /**
     * Начало загрузки
     */
    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(TAG,"onStartLoading");
        forceLoad();
    }

    /**
     * Сеттер для имени дома
     * @param sHouseName - имя дома
     */
    public void setHouseName(String sHouseName) {
        this.sHouseName = sHouseName;
    }
}
