package com.skillbranch.devtest.utils;

/**
 * Путь к API
 */
public interface AppConfig {
    String BASE_URL = "https://anapioficeandfire.com/api/";
    int START_DELAY =1500;
}
